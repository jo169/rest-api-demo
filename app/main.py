from typing import Union, List, Dict
from fastapi import FastAPI, HTTPException, Request, Body
from pydantic import BaseModel
import os
import datetime

app = FastAPI()

submissions = []

class PolypeptideSubmission(BaseModel):
    date : str
    name : str 
    email : str 
    institution : str 
    command : str
    content : str
    stderr : str 
    runtime : str 
    memory_use : str

@app.get("/")
def read_root():
    return {"Hello": "World"}


@app.get("/uptime")
def return_uptime():
    awake_since = os.popen('uptime -p').read()
    right_now = datetime.datetime.now()
    return {"server_uptime": awake_since,
            "current_timestamp": right_now}

#post method sends user response to PolypeptideSubmision class (turns to submission) 
#which stores the input as a dictionary. 
@app.post("/polypeptide/{peptide_string}")
def create_polypeptide(submission: PolypeptideSubmission):
    submission_data = submission.dict()
    #this line makes puts date in xxx-xx-xxx format, datetime turns str int oobject, 
    #easier to manipulate  data like this
    submission_data["date"] = datetime.datetime.fromisoformat(submission_data["date"])
    #Simply replaces the str in submission to object and adds it to dictionary
    #and adds it to empty list submissions
    submissions.append(submission_data)
    return {"message": "Submission received"}


'''
response model gives us expected format since we turn str to object
if submissions is empty return nothing 
else return submissions
'''
@app.get("/polypeptide", response_model=List[Dict[str, Union[str, datetime.datetime]]])
def read_polypeptides():
    if not submissions:
        raise HTTPException(status_code=404, detail="No submissions found")
    return submissions

